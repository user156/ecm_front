import Vue from "vue"
import Vuex from "vuex"
import user from "./modules/user"
import todo from "./modules/todo"
import commerce from "./modules/commerce"
import sns from "./modules/sns"

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    pageData: [
    ]
  },
  getters: {
    pageGetter(state) {
      return state.pageData
    }
  },
  modules: {
    user,
    todo,
    commerce,
    sns
  }
})

