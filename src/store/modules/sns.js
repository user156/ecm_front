import axios from "axios";

axios.defaults.baseURL = 'http://127.0.0.1:8000/api'
export default {
  namespaced: true,
  state: {
    // token: localStorage.getItem('access_token') || null,
    userId: [],
    insta: [],
  },
  mutations: {
    RETRIEVE_INSTA(state, insta) {
      state.insta = insta
    }
  },
  actions: {
    addInsta(context,insta) {

      return new Promise((resolve, reject) => {
        axios.get('/instaInfo?id='+insta.id)
          .then(response => {
            // context.commit('RETRIEVE_INSTA', response.data)
            resolve(response)

          })
          .catch(error => {
              reject(error)
          })
      })

    }
  },
  getters: {

  }
}
