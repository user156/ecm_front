import axios from "axios";

axios.defaults.baseURL = 'http://127.0.0.1:8000/api'
export default {
  namespaced: true,
  state: {
    token: localStorage.getItem('access_token') || null,
  },
  mutations: {
    RETRIEVE_TOKEN(state, token) {
      state.token = token
    },
    DESTROY_TOKEN(state) {
      state.token = null
    },
  },
  actions: {
    retrieveName(context) {
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
      return new Promise((resolve, reject) =>{
        axios.get('/user')
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    register(context, data) {
      return new Promise((resolve, reject) =>{
        axios.post('/register',{
          name: data.name,
          email: data.email,
          password: data.password,
          confirm_password: data.confirm_password,
        })
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            console.log(error)
            alert('등록이 실패했습니다.')
            reject(error)
          })
      })
    },
    destroyToken(context) {
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
      if(context.getters.loggedIn) {
        return new Promise((resolve, reject) =>{
          axios.post('/logout')
            .then(response => {
              localStorage.removeItem('access_token')
              context.commit('DESTROY_TOKEN')
              resolve(response)
            })
            .catch(error => {
              localStorage.removeItem('access_token')
              context.commit('DESTROY_TOKEN')
              reject(error)
            })
        })
      }
    },
    retrieveToken(context, credentials) {
      return new Promise((resolve, reject) =>{
        axios.post('/login', {
          email: credentials.username,
          password: credentials.password,
        })
          .then(response => {
            const token = response.data.access_token
            const userId = response.data.user_info.id
            const name = response.data.user_info.name
            localStorage.setItem('access_token', token)
            localStorage.setItem('userId', userId)
            localStorage.setItem('name', name)
            context.commit('RETRIEVE_TOKEN', token)

            //다른 모듈에 커밋 root:true를 안하면 하위에서 찾기 때문에 경로를 루트에서 시작되게 하는것
            context.commit('todo/RETRIEVE_TOKEN', token, {root: true})
            context.commit('commerce/RETRIEVE_TOKEN', token, {root: true})
            context.commit('commerce/USER_INFO', { userId : userId, name:name }, {root: true})

            // context.commit('commerce/USER_INFO', { userId : userId, name:name }, {root: true})
            // console.log('로그인결과값', response)
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
  },
  getters: {
    loggedIn(state) {
      return state.token !== null
    },
  }
}
