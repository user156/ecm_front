import axios from "axios";

axios.defaults.baseURL = 'http://127.0.0.1:8000/api'
export default {
  namespaced: true,
  state: {
    url: 'http://localhost:8080/',
    storageDir : 'http://localhost:8000/storage/',
    products: '',
    token: localStorage.getItem('access_token') || null,
    userId : localStorage.getItem('userId'),
    name : localStorage.getItem('name'),
  },
  mutations: {
    RETRIEVE_TOKEN(state, token) {
      state.token = token
    },
    USER_INFO(state, userInfo) {
      // console.log('USER_INFO', userInfo)
      state.userId = userInfo.userId
      state.name = userInfo.name
      console.log(state.userId)
      console.log(state.name)
    },
  },
  actions: {
    landingProduct(context,insta) {
      return new Promise((resolve, reject) => {
        axios.get('/landingProduct')
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    shopProduct(context, data) {
      return new Promise((resolve, reject) => {
        axios.get('/shop?category='+data.category+'&page=' + data.page+'&sort=' + data.sort, {})
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    showProduct(context, data) {
      return new Promise((resolve, reject) => {
        axios.get('/show/' + data.id, {

        })
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    loggedInCartAdd(context, product) {
      // console.log(product)

      return new Promise((resolve, reject) => {
        axios.post('/cart', {
          userId: context.state.userId,
          productId: product.productId,
          cnt: product.cnt,
        })
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
  },
  getters: {

  }
}
