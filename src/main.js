// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import Master from './components/layouts/Master'
import { store } from './store/store'
import VeeValidate from 'vee-validate'
import CxltToastr from 'cxlt-vue2-toastr'
import InstantSearch from 'vue-instantsearch';
import VueTailwind from 'vue-tailwind'
import './assets/css/tailwind.css'
import VueDaumPostcode from "vue-daum-postcode"
import pagination from "laravel-vue-pagination"
import { won } from "./filters/index"


import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'




const toastrConfigs = {
  position: 'bottom right',
  showDuration: 2000,
  timeOut: 5000,
  progressBar: true,
}




window.eventBus = new Vue()
Vue.filter('won',won)
Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(VeeValidate)
Vue.use(CxltToastr, toastrConfigs)
Vue.use(InstantSearch)
Vue.use(VueTailwind)
Vue.use(VueDaumPostcode)
Vue.use(pagination)
//number format 필터




Vue.use(FontAwesomeIcon)



Vue.component('font-awesome-icon', FontAwesomeIcon)


const router = new VueRouter({
  routes,
  mode:'history'
})

router.beforeEach((to, from, next) => {
  // if(to.name == 'notFound') {
  //   next({
  //     name: 'NotFound',
  //   })
  // }
  if (to.matched.some(record => record.meta.requiresAuth)) {

    // 이 라우트는 인증이 필요하며 로그인 한 경우 확인하십시오.
    // 그렇지 않은 경우 로그인 페이지로 리디렉션하십시오.
    if (!store.getters['user/loggedIn']) {
      next({
        name: 'login',
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.requiresVisitor)) {
    // 이 라우트는 인증이 필요하며 로그인 한 경우 확인하십시오.
    // 그렇지 않은 경우 로그인 페이지로 리디렉션하십시오.
    if (store.getters['user/loggedIn']) {
      next({
        name: 'todo',
      })
    } else {
      next()
    }
  } else {
    next() // 반드시 next()를 호출하십시오!
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router: router,
  store: store,
  components: { Master },
  template: '<Master/>',
})
