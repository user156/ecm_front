// import LandingPage from "./components/marketing/LandingPage";
import EcommerceLanding from "@/components/ecommerce/EcommerceLanding";
import EcommerceShop from "@/components/ecommerce/EcommerceShop";
import Login from "@/components/auth/Login";
import Logout from "@/components/auth/Logout";
import Register from "@/components/auth/Register";
import NotFound from "@/components/auth/NotFound";
import EcommerceProduct from "@/components/ecommerce/EcommerceProduct";


import App from "./App";


const routes = [
  {
    path: '/',
    name: 'home',
    component:EcommerceLanding
  },
  {
    path: '/todo',
    name: 'todo',
    component:App,
    meta: {
      requiresAuth: true,
    }
  },
  {
    path: '/shop',
    name: 'shop',
    component:EcommerceShop
  },
  {
    path: '/login',
    name: 'login',
    component:Login,
    props: true,
    meta: {
      requiresVisitor: true,
    }
  },
  {
    path: '/register',
    name: 'register',
    component:Register,
    meta: {
      requiresVisitor: true,
    }
  },
  {
    path: '/logout',
    name: 'logout',
    component:Logout
  },
  {
    path: '/myinfo',
    name: 'myinfo',
    component:Logout
  },
  {
    path: '/show/:id',
    name: 'show',
    component:EcommerceProduct
  },
  {
    path: '*',
    name: 'NotFound',
    component:NotFound,
    meta: {
      plainLayout: true,
    },
  },
]

export default routes
