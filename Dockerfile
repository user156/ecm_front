FROM node:12.16.3
# copy both 'package.json' and 'package-lock.json' (if available)
COPY package*.json ./
#RUN npm update
RUN npm install -g http-server
RUN npm install
#RUN npm install --production

RUN node node_modules/node-sass/scripts/install.js
# install project dependencies
#RUN npm install --production
COPY . .
RUN npm run build

EXPOSE 80
CMD ["http-server", "dist", "-p", "3000"]
